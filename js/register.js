// Function to register  valid users in localstorge
function registerUser() {
    //input field
    let fullname = document.getElementById("fullname").value;
    let email = document.getElementById("email").value;
    let username = document.getElementById("username").value;
    let password = document.getElementById("password").value;
    let confirmPassword = document.getElementById("confirm-password").value;

    let isValidFullName = false;
    let isValidUserName = false;
    let isValidEmail = false;
    let isValidPassword = false;
    let isPasswordSame = false;

    let emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let fullNameRegex = /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/;
    let passwordRegex = /^([a-z0-9]*[a-z]){3,}[a-z0-9]*$/;
    let userNameRegex = /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/;

    //Check if password match
    if (password !== confirmPassword) {
        let isPasswordSame = false;
        document.getElementById("label-confirm-password").innerHTML =
            "Password do not match";
    } else {
        isPasswordSame = true;
    }
    //Check if email is valid
    if (emailRegex.test(email)) {
        isValidEmail = true;
    } else {
        isValidEmail = false;
        document.getElementById("label-email").innerHTML =
            validator.email.message;
    }
    // check if full name is valid
    if (fullNameRegex.test(fullname)) {
        isValidFullName = true;
    } else {
        isValidFullName = false;
        document.getElementById("label-fullname").innerHTML =
            validator.name.message;
    }

    //Check if password is valid
    if (passwordRegex.test(password)) {
        isValidPassword = true;
    } else {
        isValidPassword = false;
        document.getElementById("label-password").innerHTML =
            validator.password.message;
    }

    //Check if username is valid and previously not used by anyone
    if (userNameRegex.test(username)) {
        let storedUser = localStorage.getItem("user");
        if (storedUser !== null) {
            let storedUser = localStorage.getItem("user");

            let allUser = JSON.parse(storedUser);
            let totalUser = allUser.length;
            for (i = 0; i < totalUser; i++) {
                if (allUser[i].username === username) {
                    isValidUserName = false;
                    document.getElementById("label-username").innerHTML =
                        "Username is already taken";
                    break;
                } else isValidUserName = true;
            }
        } else isValidUserName = true;
    } else {
        isValidUserName = false;
        document.getElementById("label-username").innerHTML =
            validator.username.message;
    }

    //Create object for storing users
    let userCurrent = [{
        fullname: fullname,
        email: email,
        username: username,
        password: password
    }];

    if (
        isValidEmail &&
        isPasswordSame &&
        isValidPassword &&
        isValidUserName &&
        isValidFullName
    ) {
        let user = localStorage.getItem("user");
        if (user !== null) {
            let allUser = JSON.parse(user);
            allUser = allUser.concat(userCurrent);
            localStorage.setItem("user", JSON.stringify(allUser));
            document.location = "./index.html";
        } else {
            localStorage.setItem("user", JSON.stringify(userCurrent));
            document.location = "./index.html";
        }
    } else {
        console.log(isValidEmail);
        console.log(isValidFullName);
        console.log(isValidUserName);
        console.log(isValidPassword);
        console.log(isPasswordSame);
    }
}

//Clear error mesage

document.getElementById("fullname").addEventListener("keyup", () => {
    document.getElementById("label-fullname").innerHTML = "";
});

document.getElementById("email").addEventListener("keyup", () => {
    document.getElementById("label-email").innerHTML = "";
});

document.getElementById("username").addEventListener("keyup", () => {
    document.getElementById("label-username").innerHTML = "";
});

document.getElementById("password").addEventListener("keyup", () => {
    document.getElementById("label-password").innerHTML = "";
});

document.getElementById("confirm-password").addEventListener("keyup", () => {
    document.getElementById("label-confirm-password").innerHTML = "";
});

//object to check validity of input field
const validator = {
    email: {
        regex: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        message: "Email is invalid"
    },
    name: {
        regex: /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/,
        message: "Name is invalid"
    },
    password: {
        regex: /^(?=.*d).{4,}$/,
        message: "Password must be more than  3 digits long."
    },
    username: {
        regex: /^.{4,}$/,
        message: " Username must be atleast 4 characters long"
    }
};