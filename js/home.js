let user = localStorage.getItem("loggedUser");
if (user === null || user === "") {
  document.location = "./index.html";
}
let myQuestions = [
  {
    question: "Who among below has won least Champions League title?",
    options: {
      a: "Cristiano ",
      b: "Messi",
      c: "Neymar",
      d: "Hazard"
    },
    correctAnswer: "Hazard"
  },
  {
    question: "Who won 2018 footboall worldcup?",
    options: {
      a: "Portugal",
      b: "Croatia",
      c: "England",
      d: "France"
    },
    correctAnswer: "France"
  },
  {
    question: "Who is the captain of Nepali Cricket team?",
    options: {
      a: "Paras",
      b: "Gynendra",
      c: "Deuba",
      d: "Kp Oli"
    },
    correctAnswer: "Gynendra"
  },
  {
    question: "What is capital of Britian?",
    options: {
      a: "London",
      b: "Manchester",
      c: "Chelsea",
      d: "Liverpool"
    },
    correctAnswer: "London"
  }
];

document.getElementById("logout").addEventListener("click", () => {
  localStorage.setItem("loggedUser", "");
  document.location = "./index.html";
});

function myFunction(name, id) {
  document.getElementById("form-question").style.pointerEvents = "none";
  let label = "";
  switch (id) {
    case "option1":
      label = "op1";
      break;
    case "option2":
      label = "op2";
      break;
    case "option3":
      label = "op3";
      break;
    case "option4":
      label = "op4";
      break;
    default:
      break;
  }
  if (name === myQuestions[next].correctAnswer) {
    document.getElementById(label).style.backgroundColor = "green";

    score += 1;
  } else {
    document.getElementById(label).style.backgroundColor = "red";

    for (i = 1; i <= 4; i++) {
      let potentialAnswer = document.getElementById("option" + i).value;
      if (potentialAnswer === myQuestions[next].correctAnswer) {
        document.getElementById("op" + i).style.backgroundColor = "green";
      }
    }
    document.getElementById(label).style.backgroundColor = "red";
  }
  let len = myQuestions.length;
  console.log(next);
  next += 1;
  console.log(next);
  console.log(myQuestions.length);

  if (next >= myQuestions.length) {
    setTimeout(() => {
      document.getElementById(
        "root"
      ).innerHTML = `<p class='score'>Your score  <br> <span class='score-value'>${score}<span><p> <div id='play-again' onClick=playAgain()>Play Again</div>`;
      document.getElementById(
        "user"
      ).innerHTML = `Game Finished ${localStorage.getItem("loggedUser")}`;
      document.getElementById("descp").innerHTML =
        "Click on Play Again button to try one more time";
    }, 1000);
  } else {
    document.getElementById("play-next").style.display = "block";
  }
}

function playGame(next) {
  document.getElementById(
    "root"
  ).innerHTML = `<div><p class='question'>Question${next + 1} : ${
    myQuestions[next].question
  }<p></div>
    <form class='questionare' id='form-question'>
<div class='option-top'>
        <div class='option' id='op1'>
        <input type="radio" id="option1" name="options" value="${
          myQuestions[next].options["a"]
        }"  onClick='myFunction(value,id)'>
      <label for="option1" id='label1'>${
        myQuestions[next].options.a
      }</label><br>
        </div>
        <div class='option' id='op2'>
     <input type="radio" id="option2" name="options" value="${
       myQuestions[next].options["b"]
     }" onClick='myFunction(value,id)'>
      <label for="option2" id='label2'>${
        myQuestions[next].options.b
      }</label><br>

    </div>
    </div>
  <div class= 'option-bottom'>
        <div class='option' id='op3'>
        <input type="radio" id="option3" name="options" value="${
          myQuestions[next].options["c"]
        }"  onClick='myFunction(value,id)'>
      <label for="option3" id='label3'>${
        myQuestions[next].options.c
      }</label><br>
        </div>
        <div class='option' id='op4'>
     <input type="radio" id="option4" name="options" value="${
       myQuestions[next].options["d"]
     }" onClick='myFunction(value,id)'>
      <label for="option4" id='label4'>${
        myQuestions[next].options.d
      }</label><br>
    </div>
    </div>
    </form>
    <div id='play-next' onClick='playNextFunction()'>Next Question</div>`;
}
next = 0;
score = 0;
playGame(0);

function playAgain() {
  location.reload();
}

function playNextFunction() {
  playGame(next);
}
document.getElementById("user").innerHTML =
  "Welcome " + localStorage.getItem("loggedUser");
