//Check if user is already loggedin
let user = localStorage.getItem("loggedUser");
if (user !== null && user !== "") {
    if (localStorage.getItem("user") !== null) {
        document.location = "./home.html";
    }
}
//Login credebtial validation
document.getElementById("submit").addEventListener("click", () => {
    let username = document.getElementById("username").value;
    let password = document.getElementById("password").value;
    let storedUser = localStorage.getItem("user");
    if (storedUser !== null) {
        let allUser = JSON.parse(storedUser);
        let totalUser = allUser.length;
        for (i = 0; i < totalUser; i++) {
            if (
                username === allUser[i].username &&
                password === allUser[i].password
            ) {
                localStorage.setItem("loggedUser", allUser[i].fullname);
                document.location = "./home.html";
            } else
                document.getElementById("error-block").innerHTML =
                "Enter valid username and password";
        }
    } else {
        console.log("password milena");
        document.getElementById("error-block").innerHTML =
            "User is not registered";
    }
});

//Switch between login form and registration formm
document.getElementById("goto-registration").addEventListener("click", () => {
    document.location = "./register.html";
});